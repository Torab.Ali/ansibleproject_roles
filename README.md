**Ansible Playbook for Service Provisioning using ROLES (Branch: master)**

**Overview**
This Ansible playbook automates the process of provisioning a service on servers running CentOS and Ubuntu operating systems. The playbook is organized using Ansible roles to simplify the playbook content and make it more manageable.

**Project Structure**

**playbook.yml:** This is the main Ansible playbook file that orchestrates the provisioning tasks.

**ansible.cfg:** Ansible configuration file.

**hostfile: **Inventory file containing details of target servers.

**Ansible Roles**

The playbook utilizes Ansible roles to organize and segregate different components of the provisioning process. Ansible roles provide a structured approach to managing variables, tasks, files, templates, and handlers.

**Role Structure**

When you initialize a new role using the ansible-galaxy init command, it creates a directory structure with the following directories:

**vars:** Contains variable files for storing role-specific variables.

**templates:** Stores template files used for generating configuration files dynamically.

**tasks:** Holds task files that define the actions to be performed by the role.

**handlers:** Contains handler files that define actions triggered by events during playbook execution.

**files:** Stores static files that need to be copied to target servers.

**defaults:** Contains default variable values for the role.

**Usage**

**Create Roles:** Create roles for different components of the provisioning process using the ansible-galaxy init command.

**Organize Playbook Content:** Move playbook content into the appropriate directories within the roles.

Move variables to the vars directory.

Copy files and templates to their respective directories.

Define tasks in the tasks/main.yml file.

Define handlers in the handlers/main.yml file.

**Run the Playbook:** Execute the playbook using the ansible-playbook command, specifying the playbook.yml file and inventory file.

```
**bash**
**ansible-playbook -i hostfile playbook.yml**
```


**Benefits of Using Ansible Roles**

**Simplicity:** Roles simplify playbook content by organizing tasks, variables, and files into separate directories.

**Reusability:** Roles can be reused across different playbooks and projects, promoting code reuse and modularity.

**Maintainability:** Roles make it easier to maintain and update provisioning tasks, as each role focuses on a specific aspect of the configuration.

**License**

This project is licensed under the MIT License.